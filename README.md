# WGA_PBSIM 

WGA_PBSIM.py is a small python3.x program that simulates whole genome amplified PacBio reads in parallel.
---

## Prerequisites/dependencies

1. PBSIM
2. clustalw
3. Biopython > v1.70
4. numpy

---

## Install
To install PBSIM see: https://github.com/pfaucon/PBSIM-PacBio-Simulator
Make sure PBSIM is executable as pbsim in your $PATH variable. (Might want to use a link)

To install clustalw:

	sudo apt-get install clustalw
	
To install Biopython:

	pip3 install biopython

To install numpy:

	pip3 install numpy

## Run WGA_PBSIM

**!NOTE: If you want to make use of the lognormal distribution then please check the images in the Image folder to see how these settings work!**

First test if the program works:

	cd /path_to_WGA_PBSIM_download
	./WGA_PBSIM.py

This should return usage.

Next, try:

	./WGA_PBSIM.py --help
	
and the following should pop up:

    optional arguments:
    -h, --help            show this help message and exit
    -r REFERENCE, --reference REFERENCE
                        Absolute path to a reference fasta. Multifasta will 
                        be concatenated, but reads will never overlap 
                        fasta records'(default:
                        os.getcwd())
    -o OUTPUT, --output OUTPUT
                        Absolute path to output file. (default:
                        os.getcwd())
    -p PREFIX, --prefix PREFIX
                        Prefix for filenames (default: )
    --ratio RATIO         Ratio of WGA artefacts in output, please provide
                        according example: normal:simple:multiple:invrep, so
                        default is 1:1:1:0 (default: 1:1:1:0)
    -d {UNIFORM,LOGNORMAL}, --distribution_type {UNIFORM,LOGNORMAL}
                        Type of distribution the lengths of the reads should
                        be sampled of. (default: UNIFORM)
    --distribution_low DISTRIBUTION_LOW
                        Set lower limit for distribution. (default: 1000)
    --distribution_high DISTRIBUTION_HIGH
                        Set upper limit for distribution. (default: 10000)
    --set_size SET_SIZE   Number of reads to be created. (default: 10000)
    --min_pal MIN_PAL     A lower limit for the percentage taken from a read
                        that is extended with its reverse complement and
                        together is called a palindrome. (default: 1)
    --max_pal MAX_PAL     An upper limit for the percentage taken from a read
                        that is extended with its reverse complement and
                        together is called a palindrome. (default: 25)
    --min_multiplier MIN_MULTIPLIER
                        A lower limit for the number of times a palindrome can
                        be repeated. (default: 2)
    --max_multiplier MAX_MULTIPLIER
                        An upper limit for the number of times a palindrome
                        can be repeated. (default: 10)
    -n NUMBER_PROCESSES, --number_processes NUMBER_PROCESSES
                        Number of processes to be used, ideally equal to
                        number of CPUs to be used. (default: 2)
    --mean MEAN           Mean of the normal distribution that is underneath the
                        lognormal distribution. Only required when using
                        --distribution-type LOGNORMAL. (default: 9.0)
    --sigma SIGMA         Sigma of the normal distribution that is underneath
                        the lognormal distribution. Only required when using
                        --distribution-type LOGNORMAL. (default: 1.0)
    --transformation TRANSFORMATION
                        Shift to left or right depending on the settings used
                        for --mean and --sigma. Make sure to check
                        documentation. Negative values will be filtered out.
                        (default: 0)
    --model_path MODEL_PATH
                        The path to the standardmodel file of PBSIM. Default =
                        /<your_pbsim_install_dir>/data/model_qc_clr,common
                        example: /usr/local/bin/PBSIM-PacBio-
                        Simulator/data/model_qc_clr (default:
                        /usr/local/bin/PBSIM-PacBio-
                        Simulator/data/model_qc_clr)



For most of the arguments default values are set like above. 

Two arguments are mandatory:
-r and --set_size.

Minimal example:

    ./WGA_PBSIM.py -r <path_to_reference_fasta> --set_size
    # This will drop the output in the current working directory.


   