#!/usr/bin/env python3
"""
This simple program is designed for the purpose of knitting different programs together as a
pipeline. Therefore it is purpose specific and in no way OO. It works on N processing units at a
time.
It runs through the following steps.\n
1. Create a distribution S of lengths with average length l and n as the number of datapoints.\n
2. Get a piece p from the reference genome of length l taken from S.\n
3. Push p through Readmodifier and either add simple palindrome, multi palindrome, inverted\n
repeat or do nothing to the read. [Nx]\n
4. Write read to file in /temp and to a collection file for backtrace purposes.\n
5. Run PBSIM. [Nx]\n
6. Find out which read from the output of PBSIM is suitable and contains the inserted palindromes. [Nx]\n
7. Analyze exactly what modifications have been done to this sequence [Nx].\n
8. Add read to bigger fasta file.\n
9. cleanup temp files.\n
"""

from ReadModifier import ReadModifier
import argparse
import os
import itertools
import bisect
import random as rn
import subprocess
import sys
import numpy
import time
import shutil
from queue import Empty
from Bio import AlignIO
from Bio import SeqIO
from Bio.Align.Applications import ClustalwCommandline
from Bio.Seq import MutableSeq, Seq
from multiprocessing import Manager, Process

sys.path.append('~/bin')

__author__ = "Ronald Nieuwenhuis"
__copyright__ = "None"
__credits__ = ["Ronald Nieuwenhuis", "Sven Warris"]
__license__ = "GPL"
__maintainer__ = "Ronald Nieuwenhuis"
__email__ = "ronald.nieuwenhuis@wur.nl"
__status__ = "Development"
__date__ = "2018-02-01"
__version__ = "0.1"

# Two global variables we need. Not pythonic to instantiate thme already but for convenience.
weighted_categories = None
model_path = None


def check_ratios(ratio):
    """Check is the ratios given on the commandline can be interpreted as integers and must be
    equal or greater than 0."""
    parts = ratio.split(":")
    if len(parts) != 4:
        return False
    else:
        try:
            int_parts = [int(x) for x in parts]
        except ValueError:
            return False
    if all(isinstance(x, int) for x in int_parts) and all(x >= 0 for x in int_parts):
        global weighted_categories
        weighted_categories = [("normal", int_parts[0]), ("simple", int_parts[1]),
                               ("multiple", int_parts[2]), ("invrep", int_parts[3])]
        return True
    else:
        return False


def parse_commandline_arguments():
    """Parses the commandline and returns Namespace object."""
    parser = argparse.ArgumentParser(prog='WGA_PBSIM.py', description=__doc__,
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-r', '--reference',
                        help='Absolute path to a reference fasta. Multifasta will be concatenated,'
                             ' but reads will never overlap fasta records',
                        required=True,
                        default=os.getcwd())
    parser.add_argument('-o', '--output',
                        help='Absolute path to output file.',
                        required=False,
                        default=os.getcwd())
    parser.add_argument('-p', '--prefix',
                        help='Prefix for filenames',
                        required=False,
                        default='',
                        type=str)
    parser.add_argument('--ratio',
                        help='Ratio of WGA artefacts in output, please provide according example: '
                             'normal:simple:multiple:invrep, so default is 1:1:1:0',
                        required=False,
                        default='1:1:1:0',
                        type=str)
    parser.add_argument('-d', '--distribution_type',
                        help='Type of distribution the lengths of the reads should '
                             'be sampled of.',
                        required=False,
                        default='UNIFORM',
                        choices=['UNIFORM', 'LOGNORMAL'])
    parser.add_argument('--distribution_low',
                        help='Set lower limit for distribution.',
                        required=False,
                        default=1000,
                        type=int)
    parser.add_argument('--distribution_high',
                        help='Set upper limit for distribution.',
                        required=False,
                        default=10000,
                        type=int)
    parser.add_argument('--set_size',
                        help='Number of reads to be created.',
                        required=True,
                        default=10000,
                        type=int)
    parser.add_argument('--min_pal',
                        help='A lower limit for the percentage taken from a read that '
                             'is extended with its reverse complement and together is called '
                             'a palindrome.',
                        required=False,
                        default=1,
                        type=int)
    parser.add_argument('--max_pal',
                        help='An upper limit for the percentage taken from a read that '
                             'is extended with its reverse complement and together is called '
                             'a palindrome.',
                        required=False,
                        default=25,
                        type=int)
    parser.add_argument('--min_multiplier',
                        help='A lower limit for the number of times a palindrome'
                             ' can be repeated.',
                        required=False,
                        default=2,
                        type=int)
    parser.add_argument('--max_multiplier',
                        help='An upper limit for the number of times a palindrome'
                             ' can be repeated.',
                        required=False,
                        default=10,
                        type=int)
    parser.add_argument('-n', '--number_processes',
                        help='Number of processes to be used, ideally equal to number of CPUs to '
                             'be used.',
                        required=False,
                        default=len(os.sched_getaffinity(0)),
                        type=int)
    parser.add_argument('--mean',
                        help='Mean of the normal distribution that is underneath the lognormal '
                             'distribution. Only required when using --distribution-type '
                             'LOGNORMAL.',
                        required=False,
                        default=9.0,
                        type=float)
    parser.add_argument('--sigma',
                        help='Sigma of the normal distribution that is underneath the lognormal '
                             'distribution. Only required when using --distribution-type '
                             'LOGNORMAL.',
                        default=1.0,
                        type=float)
    parser.add_argument('--transformation',
                        help='Shift to left or right depending on the settings used for --mean and'
                             ' --sigma. Make sure to check documentation. Negative values will be '
                             'filtered out.',
                        default=0,
                        type=int)
    parser.add_argument('--model_path',
                        help='The path to the standardmodel file of PBSIM. '
                             'Default = /<your_pbsim_install_dir>/data/model_qc_clr,'
                             'common example: /usr/local/bin/PBSIM-PacBio-Simulator/'
                             'data/model_qc_clr',
                        required=False,
                        default='/usr/local/bin/PBSIM-PacBio-Simulator/data/model_qc_clr',
                        type=str)
    args = parser.parse_args()
    if len(args.prefix.split('_')) > 1:
        raise argparse.ArgumentError(message='The use of underscores in the prefix is not allowed'
                                             '.', argument=None)
    if args.min_multiplier < 2:
        raise argparse.ArgumentError(message='Given value: {}, is not valid. Argument '
                                             '--min-multiplier '
                                             'should be >= 2.'.format(str(args.min_multiplier)),
                                     argument=None)
    if args.max_multiplier > 10:
        raise argparse.ArgumentError(message='Given value: {}, is not valid. Argument '
                                             '--max-multiplier '
                                             'should be <= 10.'.format(str(args.max_multiplier)),
                                     argument=None)
    if args.number_processes > len(os.sched_getaffinity(0)):
        raise argparse.ArgumentError(message='Given value: {}, for number of processes exceeds '
                                             'the number of CPUs in your machine, this is '
                                             'ineffective and therefore discouraged by this '
                                             'error message. \n To find out how many CPUs you have '
                                             'type "nproc" in your terminal.'.format(
            str(args.number_processes)), argument=None)
    if args.min_pal < 1:
        raise argparse.ArgumentError(message='Given value: {} is not a valid input. '
                                             '--min_pal should be >= 1.'.format(str(args.min_pal)),
                                     argument=None)
    if args.distribution_type == 'LOGNORMAL':
        print('NOTICE: Because you used --distribution_type LOGNORMAL, --distribution_low and '
              '--distribution_high low may cause a deviation from a lognormal distribution. '
              'However it is advised to use a maximum because there is a probability extreme long '
              'reads roll out.')
    if args.distribution_type == 'UNIFORM':
        print('NOTICE: Because you used --distribution_type UNIFORM, --mean, --sigma and '
              '--transformation will be omitted because they are not associated with uniform '
              'distribution. If you did not set them, dismiss this message.')
    if not 8.0 <= args.mean <= 9.5:
        raise argparse.ArgumentError(message='The chosen mean for the lognormal distribution does '
                                             'not result in a typical distribution for PacBio '
                                             'sequencing. Please choose a value between 8.0 and '
                                             '9.5.For more information see the documentation.',
                                     argument=None)
    if not 0.25 <= args.sigma <= 1.5:
        raise argparse.ArgumentError(message='The chosen mean for the lognormal distribution does '
                                             'not result in a typical distribution for PacBio '
                                             'sequencing. Please choose a value between 8.0 and '
                                             '9.5. For more information see the documentation.',
                                     argument=None)
    if not os.path.isfile(args.model_path):
        raise argparse.ArgumentError(message='You model file does not exist, please use '
                                             '--model_path to give the path to the model_qc_clr '
                                             'file in your pbsim installation directory',
                                     argument=None)
    else:
        global model_path
        model_path = args.model_path
    if not check_ratios(args.ratio):
        raise argparse.ArgumentError(message="The string for the --ratio option does not meet "
                                             "requirements. Please use a format like 10:20:1:0",
                                     argument=None)
    return args


def concat_ref_fasta(reference_fasta_list):
    """Concatenate the reference fasta into one big fasta, but introduce N as separators, this way
    no cross chromosome reads will occur."""
    first_rec = reference_fasta_list[0]
    for seq_rec in reference_fasta_list[1:]:
        first_rec.seq = first_rec.seq + 'N' + seq_rec.seq
    return first_rec


def wga_pbsim():
    """This is the m    ain program, tried to keep as compact and readable as possible. For more info
    see __doc__"""
    try:
        arguments = parse_commandline_arguments()
    except argparse.ArgumentError as AE:
        print(AE.message)
        sys.exit(10)
    if not (shutil.which('pbsim') and shutil.which('clustalw')):
        print('pbsim and clustalw should be in your $PATH.')
        sys.exit(11)

    # Some instances we need.
    temp = '/tmp/'
    all_modified_reads = arguments.output + '/All_modified_reads_' + arguments.prefix + '.fasta'
    all_customized_reads = (arguments.output + '/All_customized_reads_' + arguments.prefix +
                            '.fasta')
    reference = arguments.reference
    min_perc = arguments.min_pal
    max_perc = arguments.max_pal
    min_multi = arguments.min_multiplier
    max_multi = arguments.max_multiplier
    number_procs = arguments.number_processes

    # Do the whole process with these files opened.
    with open(reference) as ref_handle, open(all_modified_reads, mode='w') as mod_handle, \
            open(all_customized_reads, mode='w') as custom_handle:

        # Check for multifasta reference and handle it if true
        reference_fasta_list = list(SeqIO.parse(ref_handle, 'fasta'))
        if len(reference_fasta_list) > 1:
            reference_fasta = concat_ref_fasta(reference_fasta_list)
        else:
            reference_fasta = reference_fasta_list[0]
        reference_length = len(reference_fasta.seq)
        lengths_distribution = get_distribution(arguments.distribution_type,
                                                arguments.set_size,
                                                arguments.distribution_low,
                                                arguments.distribution_high,
                                                arguments.mean,
                                                arguments.sigma,
                                                arguments.transformation)

        # Things needed for multiprocessing
        manager = Manager()
        results = manager.Queue(arguments.set_size)
        work_to_do = manager.Queue(number_procs)

        pool = []
        for i in range(number_procs):
            p = Process(target=create_customized_read, args=(work_to_do, results))
            p.start()
            pool.append(p)

        for p in pool:
            print(p)

        # This iterator is the basis: every iteration creates 1 read and puts it in the queue.
        for i in range(1, len(lengths_distribution) + number_procs):
            if i <= len(lengths_distribution):
                length = lengths_distribution[i - 1]
                found_clean_seq = False
                while not found_clean_seq:
                    start = rn.randint(0, reference_length - length)
                    no_n_in_seq = True
                    print(no_n_in_seq)
                    while no_n_in_seq:
                        piece_of_ref = reference_fasta.seq[start:start + length]
                        if not check_for_n(piece_of_ref):
                            no_n_in_seq = False
                        item_in_queue = (i, length, min_perc, max_perc, min_multi, max_multi,
                                         reference_length, str(arguments.prefix))
                        handle = open(temp + str(i) + '.tmp', mode='w')
                        handle.write(str(piece_of_ref))
                        handle.close()
                        work_to_do.put(item_in_queue)
                        time.sleep(1)
                        found_clean_seq = True
            else:
                work_to_do.put(None)
                time.sleep(1)

        for p in pool:
            while p.is_alive():
                try:
                    record_tuple = results.get(timeout=420)
                    write_to_file(record_tuple, mod_handle, custom_handle)
                except Empty:
                    p.terminate()
            p.join(timeout=1)
        print("Finished")


def write_to_file(records, mod_handle, custom_handle):
    """Picks tuple from queue and writes [0] and [1] to separate fasta files."""
    SeqIO.write(records[0], mod_handle, 'fasta')
    SeqIO.write(records[1], custom_handle, 'fasta')


def check_for_n(piece_of_ref):
    """Checks the sequence for N chars, if included in seq return False."""
    seq = piece_of_ref.upper()
    if 'N' in seq:
        return False


def create_customized_read(in_from_queue, out_list):
    """This method makes calls on multiple other methods to do the work. It contains 2 while loops
    and makes calls on subprocess twice. It gets its information from a Queue object and also
    returns its results into a different Queue object."""
    while True:
        temp = '/tmp/'
        item_from_queue = in_from_queue.get()

        # If queue is empty, it returns a None, just return.
        if item_from_queue is None:
            return
        i, length, min_perc, max_perc, min_multi, max_multi,\
            reference_length, prefix = item_from_queue
        handle = open(temp + str(i) + '.tmp')
        piece_of_ref = handle.readline()
        restart_process = False  # Switch set/reset in case the pbsim created data is a dead end.
        piece_of_ref = SeqIO.SeqRecord(Seq(piece_of_ref))
        # This while loop is restarted when the noisified reads created by PBSIM do not contain
        # the features we have inserted in the custom read.
        while not restart_process:
            # Specify four categories a read can be modified to.
            cats, weights = zip(*weighted_categories)
            cumulative_dist = list(itertools.accumulate(weights))
            edge = rn.random() * cumulative_dist[-1]
            chosen_category = cats[bisect.bisect(cumulative_dist, edge)]
            if chosen_category == 'simple':
                simple_pal_read = ReadModifier(piece_of_ref, False)
                new_read, stats = simple_pal_read.append_simple_palindrome(min_perc, max_perc)
            elif chosen_category == 'multiple':
                multi_pal_read = ReadModifier(piece_of_ref, False)
                new_read, stats = multi_pal_read.append_multi_palindromes(min_perc, max_perc,
                                                                          min_multi, max_multi)
            elif chosen_category == 'invrep':
                inv_rep_read = ReadModifier(piece_of_ref, False)
                new_read, stats = inv_rep_read.insert_inverted_repeat(min_perc, max_perc)
            else:
                new_read = piece_of_ref.seq
                stats = [len(piece_of_ref), 0] + ''.join(',' for i in range(0, 5)).split(',')

            # Write the read to fasta file with an appropriate parsable header
            new_file = temp + prefix + '_read_' + str(i) + '.fasta'
            header = '{}_{}_{}_{}_{}_{}_{}'.format(prefix,
                                                   str(i),
                                                   chosen_category,
                                                   str(stats[0]),  # Initial sequence length
                                                   str(stats[1]),  # Palindrome length
                                                   str(stats[3]),  # Multiplier (for multi only)
                                                   str(stats[2]),  # Insert point inv rep in \
                                                   # orig sequence. For other cases defaults 0
                                                   )

            new_record = SeqIO.SeqRecord(new_read, id=header, description='')

            with open(new_file, mode='w') as single_read_outfile:
                SeqIO.write(new_record, single_read_outfile, 'fasta')

            command = ['pbsim',
                       '--prefix', temp + prefix + '_read_' + str(i),
                       '--data-type', 'CLR',
                       '--depth', '1',
                       '--length-min', str(int(stats[0]) + int(stats[1])),
                       '--length-max', str(int(stats[0]) + int(stats[1]) + 2),
                       '--length-mean', str(int(stats[0]) + int(stats[1]) + 1),
                       '--length-sd', '0',
                       '--model_qc', model_path,
                       new_file]

            # This while loop can spawn the process only ten times without succeeding but is
            # broken as soon as the process results in a good sequence.
            attempts_counter = 0
            found_good_sequence = False
            out_loop = False

            while not out_loop and not (out_loop or found_good_sequence):
                try:
                    subprocess.run(command, check=True, stdout=subprocess.DEVNULL,
                                   stderr=subprocess.DEVNULL)
                except subprocess.CalledProcessError:
                    raise subprocess.CalledProcessError

                maf = list(AlignIO.parse(new_file.split('.fasta')[0] + '_0001.maf', 'maf'))
                prepare_for_alignment(maf, new_record)
                do_msa(i)
                suitable_candidates = find_best_alignment(i)
                if len(suitable_candidates) > 0:
                    found_good_sequence = True
                attempts_counter += 1
                print('Attempts: ' + str(attempts_counter))
                if attempts_counter == 10:
                    out_loop = True  # Set switch to true so it breaks out of while loop.
            if out_loop is True and found_good_sequence is False:
                print('OPNIEUW')
                continue  # Start again at the top of the while loop, so we stay in the same iteration.
            longest = {}
            for record in suitable_candidates:
                length = sum(set([True for i in record.seq if i != '-']))
                longest[length] = record.id
            best_option = int(longest[sorted(longest, reverse=True)[0]].split('%')[1])
            modifications = analyze_alignment(maf[best_option - 1])
            id_needed = "S1_" + str(best_option)
            mods_string = "_{}_{}_{}".format(str(modifications['substitution']),
                                             str(modifications['deletion']),
                                             str(modifications['insertion']))
            with open(new_file.split('.fasta')[0] + '_0001.fastq') as fastq:
                fastq_records_dict = SeqIO.to_dict(SeqIO.parse(fastq, 'fastq'))
                final_record = fastq_records_dict[id_needed]
                final_record.id = header + '_' + str(len(final_record.seq)) \
                    + mods_string + '_' + id_needed
                final_record.name = ''
                final_record.description = ''
            cleanup_temp_files(new_file.split('.fasta')[0])
            out_list.put((new_record, final_record))
            restart_process = True  # If successful, break out of the while loop.


def analyze_alignment(alignment):
    """Sort of interface method."""
    modifications = compare_sequences(alignment[0], alignment[1])
    return modifications


def cleanup_temp_files(temp_basename):
    """Remove the temporary files that are created."""
    read_number = temp_basename.split('_')[2]
    filenames = [temp_basename + '_0001.fastq', temp_basename + '_0001.maf',
                 temp_basename + '_0001.ref', temp_basename + '.fasta',
                 '/tmp/' + read_number + '.fasta', '/tmp/' + read_number + 'out.fasta',
                 '/tmp/' + read_number + '.dnd', '/tmp/' + read_number + '.tmp']
    for file in filenames:
        os.remove(file)


def compare_sequences(sequence1, sequence2):
    """Compare two sequences of equal length and count differences per category."""
    if len(sequence1) == len(sequence2):
        zipped_sequences = zip(sequence1, sequence2)
        # Create bin
        modifications_bin = {'insertion': 0, 'substitution': 0, 'deletion': 0}
        for position in zipped_sequences:
            if position[0] != position[1]:
                if position[0] == '-':
                    modifications_bin['insertion'] += 1
                elif position[1] == '-':
                    modifications_bin['deletion'] += 1
                else:
                    modifications_bin['substitution'] += 1
        return modifications_bin
    else:
        raise Exception


def do_msa(read_number):
    """Execute the clustalw command."""
    msa_command = ClustalwCommandline('clustalw', infile='/tmp/' + str(read_number) + '.fasta',
                                      outfile='/tmp/' + str(read_number) + 'out.fasta',
                                      output='FASTA', outorder='INPUT')
    msa_command()


def find_best_alignment(read_number):
    """Parse the msa output fasta and search the sequences fulfill the requirements and return
    them in a list."""
    with open('/tmp/' + str(read_number) + 'out.fasta') as handle:
        fasta = list(SeqIO.parse(handle, 'fasta'))
        all_sequences = list(zip(*[str(sequence.seq) for sequence in fasta]))
        suitable_bin = []
        all_characteristics = fasta[0].id.split('%')[0].split(
            '_')  # Characteristics are embedded in header of fasta and the same for all sequences.
        modification_type = all_characteristics[2]
        length = int(all_characteristics[3])
        if modification_type != 'normal':
            palindrome_length = int(all_characteristics[4])
        if modification_type == 'invrep':
            insertion = int(all_characteristics[6])
        for candidate in fasta[1::]:
            suitable_candidate = True
            sequence_number = int(candidate.id.split('%')[1])
            if modification_type == 'simple':
                start_palindrome = all_sequences[length]
                end_palindrome = all_sequences[length + palindrome_length - 1]
                if start_palindrome[sequence_number] == '-' \
                        or end_palindrome[sequence_number] == '-':
                    suitable_candidate = False
            elif modification_type == 'multiple':
                start_palindrome = all_sequences[length]
                end_palindrome = all_sequences[length + palindrome_length - 1]
                if start_palindrome[sequence_number] == '-' \
                        or end_palindrome[sequence_number] == '-':
                    suitable_candidate = False
            elif modification_type == 'invrep':
                start_palindrome = all_sequences[insertion - 1]
                end_palindrome = all_sequences[insertion + palindrome_length - 1]
                left_border = all_sequences[insertion
                                            - round((0.2 * length))
                                            - 1]
                right_border = all_sequences[insertion
                                             + palindrome_length
                                             + round((0.2 * length))
                                             - 1]
                if start_palindrome[sequence_number] == '-' \
                        or end_palindrome[sequence_number] == '-' \
                        or left_border == '-' \
                        or right_border == '-':
                    suitable_candidate = False
            if suitable_candidate:
                suitable_bin.append(candidate)
    return suitable_bin


def get_distribution(distribution_type, set_size, distribution_low, distribution_high, mean, sigma,
                     transformation):
    """Pick up distribution"""
    if distribution_type == 'UNIFORM':
        distribution = get_distribution_uniform(set_size, distribution_low, distribution_high)
        return distribution
    elif distribution_type == 'LOGNORMAL':
        distribution = get_distribution_lognormal(set_size, mean, sigma, transformation,
                                                  distribution_low, distribution_high)
        return distribution


def get_distribution_uniform(set_size, distribution_low, distribution_high):
    """Create a uniform distribution with given parameters and return it as list with integers."""
    distribution = list(numpy.random.uniform(float(distribution_low),
                                             float(distribution_high), set_size).astype(int))
    return distribution


def get_distribution_lognormal(set_size, mean, sigma, transformation, distribution_low,
                               distribution_high):
    """Create a lognormal distribution with given parameters and return it as list with
    integers."""
    distribution = list(numpy.random.lognormal(mean=float(mean), sigma=float(sigma),
                                               size=set_size).astype(int))
    distribution = [x + transformation for x in distribution if x > 2]
    distribution = [x for x in distribution if distribution_low < x < distribution_high]
    if len(distribution) == 0:
        print('Your distribution settings resulted in zero reads of length > 0. Please choose '
              'different settings. see documentation for further information.')
        sys.exit(12)
    return distribution


def prepare_for_alignment(maf, new_record):
    """
    Prepare the alignments for the multiple sequence alignment:
    1. Alignments are nested, put nested ones in list
    2. Create a copy with a MutableSeq object as .seq
    3. Remove dashes from the first of every alignment duo, this sequence is the part of the
    original containing dashes for inserts. So without dashes this is the part of the original
    read that is taken by PBSIM and has noise introduced
    4. Write the sequences to fasta file, first being the entire original read, given %ref as
    suffix and the modified sequences having their number as suffix
    """
    pair_alignments = [alignment for alignment in maf]
    sequence_options = [alignment[0] for alignment in pair_alignments]

    clean_options = []

    for sequence in sequence_options:
        copy = SeqIO.SeqRecord(MutableSeq(''), name=sequence.name, id=sequence.id,
                               description=sequence.description, annotations=sequence.annotations)
        cleaned_sequence = ''.join(str(sequence.seq).split('-'))
        copy = copy + cleaned_sequence
        clean_options.append(copy)
    clean_options.insert(0, new_record)
    read_number = new_record.id.split('_')[1]
    for i in range(0, len(clean_options)):
        if i == 0:
            clean_options[i].id = str(clean_options[i].id) + '%ref'
        else:
            clean_options[i].id = str(clean_options[i].id) + '%' + str(i)

    with open('/tmp/' + read_number + '.fasta', mode='w') as handle:
        SeqIO.write(clean_options, handle, 'fasta')


def main():
    wga_pbsim()


if __name__ == '__main__':
    main()
